local S = minetest.get_translator("classy")
local storage = minetest.get_mod_storage()

local function set_visual_and_params() end
local function restore_attributes() end

local classes = {}              -- KEY: c_name; VALUE: def
local p_classes = {}            -- KEY: p_name; VAL: c_name

local IS_SKILLS_ENABLED = minetest.get_modpath("skills")
-- TEMP: https://github.com/minetest/minetest/issues/14764 needed
local DEFAULT_PHYSICS = {
  speed = 1,
  speed_walk = 1,
  speed_climb = 1,
  speed_crouch = 1,
  speed_fast = 1,
  jump = 1,
  gravity = 1,
  liquid_fluidity = 1,
  liquid_fluidity_smooth = 1,
  liquid_sink = 1,
  acceleration_default = 1,
  acceleration_air = 1,
  acceleration_fast = 1,
  sneak = true,
  sneak_glitch = false,
  new_move = true
}




-- INTERNAL USE ONLY
function classy.load_player_data(p_name)
  local c_name = storage:get_string(p_name)

  if c_name == "" then return end

  -- se la classe non esiste più, pescane un'altra
  if not classes[c_name] then
    classy.print_warning("Class '" .. c_name .. "' of player '" .. p_name .. "' doesn't exist anymore. Assigning a random class if available, or nothing if it's not")
    minetest.chat_send_player(p_name, "[CLASSY] Your class has been removed from the database. Assigning a new one...")

    c_name = nil

    for name, c_data in pairs(classes) do
      if not next(c_data.prerequisites) then
        c_name = name
        break
      end

      -- TODO: controlla anche classi di cui soddisfa i requisiti
    end

    if c_name then
      classy.set_player_class(p_name, c_name)
    else
      minetest.chat_send_player(p_name, "There are no classes available, you're now class-less")
    end

  -- sennò aggiorna aspetto e parametri
  else
    set_visual_and_params(minetest.get_player_by_name(p_name), classes[c_name])
  end

  p_classes[p_name] = c_name
end





----------------------------------------------
---------------------CORE---------------------
----------------------------------------------

function classy.register_class(c_name, def)
  assert(c_name:match(":"), "[CLASSY] Your class technical name must follow the pattern mod_name:class_name in order to work!")
  assert(not classes[c_name], "[CLASSY] There's already a class called '" .. c_name .. "'!")

  local colon_idx = c_name:find(":")
  local sub_name = c_name:sub(colon_idx + 1)
  assert(sub_name:match("[%w_]+") == sub_name, "[CLASSY] Not allowed characters in class name '" .. sub_name .. "': only numbers, letters and _!")

  local class = {
    technical_name = c_name,
    mod = c_name:sub(1, colon_idx - 1),
    name = def.name or c_name,
    description = def.description or "",
    hp = def.hp,
    aspect = nil,
    physics_override = nil,
    starting_items = nil,
    prerequisites = {},
    -- optional dependencies, e.g. 3d_armor, skills etc.
    starting_armor = {},
    starting_skills = {}
  }

  if def.aspect and type(def.aspect) == "table" then
    local props = {}
    local def_props = def.aspect

    props.animation_speed = def_props.animation_speed
    props.textures = def_props.textures
    props.animations = def_props.animations
    props.visual_size = def_props.visual_size
    props.collisionbox = def_props.collisionbox
    props.stepheight = def_props.stepheight
    props.eye_height = def.eye_height

    class.aspect = props
  end

  if def.physics_override and type(def.physics_override) == "table" then
    class.physics_override = def.physics_override
  end

  if def.starting_items and type(def.starting_items) == "table" then
    class.starting_items = def.starting_items
  end

  if IS_SKILLS_ENABLED and def.starting_skills and type(def.starting_skills) == "table" then
    class.starting_skills = def.starting_skills
  end

  for k, v in pairs(def) do
    if k:sub(1,1) == "_" then
      class[k] = v
    end
  end

  classes[c_name] = class
end



function classy.unregister_class(name)
  classes[name] = nil
end

minetest.register_on_mods_loaded(function()
  classy.unregister_class = function() error "[CLASSY] Classes can be unregistered at load time only!" end
end)





----------------------------------------------
-----------------GETTERS----------------------
----------------------------------------------

function classy.get_classes(mod, as_table)
  local ret = {}

  if not mod then
    if as_table then
      ret = table.copy(classes)
    else
      for cl_name, _ in pairs(classes) do
        ret[#ret + 1] = cl_name
      end
      return ret
    end

  else
    if not as_table then
      for cl_name, cl_data in pairs(classes) do
        if cl_data.mod == mod then
          ret[#ret + 1] = cl_name
        end
      end

    else
      for k, cl_data in pairs(table.copy(classes)) do
        if cl_data.mod == mod then
          ret[k] = cl_data
        end
      end
    end
  end

  return ret
end



function classy.get_class_by_name(c_name)
  return classes[c_name]
end



function classy.get_player_class(p_name)
  return p_classes[p_name]
end





----------------------------------------------
-----------------SETTERS----------------------
----------------------------------------------

function classy.set_player_class(p_name, c_name, temporary)
  if not c_name or not classes[c_name] then
    classy.print_warning("Attempt to assign unknown class '" .. (c_name or "<nil>") .. "' to player '" .. p_name .. "'")
    return end

  if p_classes[p_name] == c_name then return end

  local class = classes[c_name]
  local player = minetest.get_player_by_name(p_name)
  local p_inv = player:get_inventory()

  if not player then
    classy.print_warning("Attempt to assign class '" .. (c_name or "<nil>") .. "' to a player who's not online ('" .. p_name .. "')")
    return end

  -- controlli vari
  --
  -- se si han i permessi
  if class.prerequisites then
    local p_privs = minetest.get_player_privs(p_name)
    for _, priv in ipairs(class.prerequisites) do
      if not p_privs[priv] then
        classy.print_info("Impossible to assign class '" .. c_name .. "' to '" .. p_name .. "': not all requirements have been satisfied")
        return
      end
    end
  end

  -- se c'è abbastanza spazio per eventuali oggetti iniziali
  if class.starting_items then
    local i_amount = #class.starting_items
    local air = 0

    for _, stack in ipairs(p_inv:get_list("main")) do
      if stack:get_name() == "" then
        air = air + 1
      end
    end

    if air < i_amount then
      -- TODO: al posto di annullare, butta a terra oggetti che non ci stanno
      classy.print_info("Impossible to assign class '" .. c_name .. "' to '" .. p_name .. "': not enough space in the inventory for the starting items! (" .. i_amount ..")")
      return
    end
  end

  local current_class = p_classes[p_name] and classes[p_classes[p_name]]

  if current_class then
    restore_attributes(player, current_class)
  end

  if not temporary then
    classy.print_info(p_name, "Class selected: "..  minetest.colorize("#fada8d", class.name))
  end

  -- aspetto e parametri
  set_visual_and_params(player, class)

  -- oggetti iniziali
  if class.starting_items then
    for _, i_name in ipairs(class.starting_items) do
      local stack = ItemStack(i_name)
      p_inv:add_item("main", stack)
      classy.print_info(p_name, "Received: " .. minetest.colorize("#fada8d", stack:get_description() .. " x" .. stack:get_count()))
    end
  end

  -- abilità iniziali
  if class.starting_skills then
    for _, sk_name in pairs(class.starting_skills) do
      p_name:unlock_skill(sk_name)
      if not temporary then
        classy.print_info(p_name, "You've unlocked the skill " .. minetest.colorize("#fada8d", skills.get_skill_def(sk_name).name))
      end
    end
  end

  p_classes[p_name] = c_name

  if not temporary then
    storage:set_string(p_name, c_name) -- TODO: fare in modo che possa accedere anche a giocanti offline? In caso mandare messaggio al connettersi
  end

  return true
end





----------------------------------------------
--------------------UTILS---------------------
----------------------------------------------

function classy.is_class(c_name)
  return classes[c_name] ~= nil
end



function classy.remove_player_class(p_name, temporary)
  local c_name = p_classes[p_name]

  if not c_name then return end

  local class = classes[c_name]
  local player = minetest.get_player_by_name(p_name)

  -- aspetto e fisica
  restore_attributes(player, class)

  p_classes[p_name] = nil

  if not temporary then
    storage:set_string(p_name, nil) -- TODO: idem come sopra
  end
end





----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------

function set_visual_and_params(player, class)
  -- aspetto fisico
  player:set_properties(class.aspect)

  -- fisica personalizzata
  if class.physics_override then
    player:set_physics_override(class.physics_override)
  end

  -- vita
  if class.hp then
    player:set_properties({hp_max = class.hp})
  end
end



function restore_attributes(player, prev_class)
  -- aspetto
  if prev_class.aspect then
    local p_model_name = player_api.get_animation(player).model

    player_api.set_model(player, "") -- reset, as player_api will instantly return if setting the same model the player currently has
    player_api.set_model(player, p_model_name)
  end

  -- fisica
  if prev_class.physics_override then
    player:set_physics_override(DEFAULT_PHYSICS)
  end

  -- vita
  if prev_class.hp then
    player:set_properties({hp_max = minetest.PLAYER_HP_MAX_DEFAULT})
  end

  -- oggetti
  if prev_class.starting_items then
    local p_inv = player:get_inventory()

    for _, i_name in ipairs(prev_class.starting_items) do
      p_inv:remove_item("main", i_name)
    end
  end

  local p_name = player:get_player_name()

  -- abilità
  if prev_class.starting_skills then
    for _, sk_name in pairs(prev_class.starting_skills) do
      p_name:remove_skill(sk_name)
    end
  end
end