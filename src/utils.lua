function classy.print_info(p_name, msg)
  if not p_name or not minetest.get_player_by_name(p_name) then
    -- se ci sono stringhe tradotte, le ripulisco
    if msg:find("\27%(T@[%w_-]+%)") then
      msg = msg:gsub("\27%(T@[%w_-]+%)", "")  -- removing \27(T@mod_name)
      msg = msg:gsub("\27F", "")
      msg = msg:gsub("\27E", "")
    end

    minetest.log("action", "[CLASSY] " .. msg)
  else
    minetest.chat_send_player(p_name, minetest.colorize("#c5f5f8", "[CLASSY] " .. msg))
  end
end



function classy.print_warning(msg)
  -- se ci sono stringhe tradotte, le ripulisco
  if msg:find("\27%(T@[%w_-]+%)") then
    msg = msg:gsub("\27%(T@[%w_-]+%)", "")  -- removing \27(T@mod_name)
    msg = msg:gsub("\27F", "")
    msg = msg:gsub("\27E", "")
  end

  minetest.log("error", "[CLASSY] [!] " .. msg)
end