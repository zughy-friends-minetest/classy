local S = minetest.get_translator("classy")



classy.register_class("classy:warrior_test", {
  name = S("Warrior"),
  description = S("Description of the warrior test class"),
  hp = 25,
  aspect = {visual_size = {x = 1.8, y = 1, z = 1}},
  physics_override = {speed = 0.8},
  starting_items = {"default:brick", "default:sword_steel"}
})


classy.register_class("classy:mage_test", {
  name = S("Mage"),
  description = S("Description of the mage test class"),
  hp = 18,
  aspect = {visual_size = {x = 1, y = 1.2}},
  physics_override = {speed = 1.3},
  starting_items = {"default:mese_crystal"}
})