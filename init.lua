local version = "0.1-dev"
local modname = "classy"
local modpath = minetest.get_modpath(modname)
local srcpath = modpath .. "/src"

classy = {}

dofile(srcpath .. "/api.lua")
dofile(srcpath .. "/player_manager.lua")
dofile(srcpath .. "/utils.lua")
dofile(srcpath .. "/tests/test_class.lua")

minetest.log("action", "[CLASSY] Mod initialised, running version " .. version)