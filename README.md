# Classy

Classes on Minetest! Easily manage characters with different characteristics

<a href="https://liberapay.com/Zughy/"><img src="https://i.imgur.com/4B2PxjP.png" alt="Support my work"/></a>  

### How to

Check out the [DOCS](DOCS.md)

### Add-ons
* [Skills](https://content.minetest.net/packages/giov4/skills/) by Giov4: add starting skills to your classes

### Want to help?
Feel free to:
* open an [issue](https://gitlab.com/zughy-friends-minetest/classy/-/issues)
* submit a merge request. In this case, PLEASE, do follow milestones and my [coding guidelines](https://cryptpad.fr/pad/#/2/pad/view/-l75iHl3x54py20u2Y5OSAX4iruQBdeQXcO7PGTtGew/embed/). I won't merge features for milestones that are different from the upcoming one (if it's declared), nor messy code
