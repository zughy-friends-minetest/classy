# Classy DOCS

## 1 Classes

Classes are an abstraction that allows games to easily manage characters with different characteristics.  

Classes can be permanent or temporary. Permanent classes are stored into the database and, once set, they're automatically assigned when players log in. They're useful e.g. for role playing games. On the contrary, temporary classes are not stored into the database, meaning they can't be automatically re-equipped when players log in. They're useful e.g. for minigames.

## 1.1 Registering and unregistering a class

In order to register a new class:
```lua
classy.register_class(c_name, def)
```

`c_name` is the technical name used to register the class, following the format `mod_name:class_name`. `class_name` can only contain letters, numbers and _, and there can't be two classes with the same name.
`def` is a table containing the following optional fields:
* `technical_name`: (string) automatically set for cross-referencing. It's equal to `c_name`
* `mod`: (string) automatically set. It's what it precedes `:` in `c_name`
* `name`: (string) a readable name of the class
* `description`: (string) a description of the class
* `hp`: (int) max hp of the class
* `aspect`: (table) the object properties to override for the player when the class is equipped. They're the same used in `player_api`'s [Model definition](https://github.com/minetest/minetest_game/blob/82b017af6b03731c2ebd320ff81a85c808054ead/game_api.txt#L495)
* `physics_override`: (table) same as `properties` but for physics overrides (as in Minetest physics overrides)
* `starting_items`: (table) a list of ItemStacks (format `{"item1", "item2"}`) automatically given to the player when equipping the class
* `prerequisites`: (table) a list of privileges (format `{"priv1", "priv2"}`) needed in order to equip this class
* `starting_skills`: (table) a list of skill names (format `{"name1", "name2"}`) automatically unlocked when equipping the class. It requires the `skills` mod

Custom fields are also possible, and they must start with `_` in order to be seen (e.g. `_foo`)

Classes can be unregistered as well, but only at load time. The function is
```lua
classy.unregister_class(name)
```

## 1.2 Getters
* `classy.get_classes(<mod>, <as_table>)`: returns the classes currently registered, optionally using `mod` as a filter. Default format is `{1 = c_name}`; however, if `as_table` is`true`, format is `{c_name = class}`
* `classy.get_class_by_name(c_name)`: returns the class associated to `c_name`, if any
* `classy.get_player_class(p_name)`: get the class name of `p_name`, if online

## 1.3 Setters
* `classy.set_player_class(p_name, c_name, <temporary>)`: sets class `c_name` to `p_name`, if online. Returns `true` on success
  * `temporary` is a boolean that, if `true`, avoids saving the change of class in the database
  * This function might fail for several reasons, e.g. not having enough space in the inventory to put the starting items. Hence the return

## 1.4 Utils
* `classy.is_class(c_name)`: returns whether the class `c_name` actually exists
* `classy.remove_player_class(p_name, <temporary>)`: removes the class of `p_name`, if online. See `set_player_class` for `temporary`
